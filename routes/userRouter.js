const express = require('express');
const router = express.Router();

const { authMiddleware } = require('../routes/middlewares/authMiddleware');
const { asyncWrapper } = require('./helpers/asyncHelpers');
const { validateOldNewPasswords } = require('./middlewares/validationMiddleware');
const { showProfile, deleteProfile, changePassword } = require('../controllers/userController');

router.route('/')
  .get(authMiddleware, asyncWrapper(showProfile))
  .delete(authMiddleware, asyncWrapper(deleteProfile));

router.patch('/password', authMiddleware, asyncWrapper(validateOldNewPasswords), asyncWrapper(changePassword));

module.exports = router;