const express = require('express');
const router = express.Router();

const { authMiddleware } = require('../routes/middlewares/authMiddleware');
const { asyncWrapper } = require('./helpers/asyncHelpers');
const { checkType } = require('./middlewares/validationMiddleware');
const { getAllTrucks, addTruck, getTruck, updateTruck, deleteTruck, assignTruck } = require('../controllers/truckController');

router.route('/')
  .get(authMiddleware, asyncWrapper(getAllTrucks))
  .post(authMiddleware, asyncWrapper(checkType), asyncWrapper(addTruck));

router.route('/:id')
  .get(authMiddleware, asyncWrapper(getTruck))
  .put(authMiddleware, asyncWrapper(checkType), asyncWrapper(updateTruck))
  .delete(authMiddleware, asyncWrapper(deleteTruck));

router.post('/:id/assign', authMiddleware, asyncWrapper(assignTruck));

module.exports = router;