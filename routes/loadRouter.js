const express = require('express');
const router = express.Router();

const { authMiddleware } = require('../routes/middlewares/authMiddleware');
const { asyncWrapper } = require('./helpers/asyncHelpers');
const { validateLoad } = require('./middlewares/validationMiddleware');
const { getLoads, addLoad, getOneLoad, updateLoad, deleteLoad, postLoad, getShippingInfo, getActiveLoad, iterateNextLoadState } = require('../controllers/loadController');

router.route('/')
  .get(authMiddleware, asyncWrapper(getLoads))
  .post(authMiddleware, asyncWrapper(validateLoad), asyncWrapper(addLoad));

router.get('/active', authMiddleware, asyncWrapper(getActiveLoad));

router.patch('/active/state', authMiddleware, asyncWrapper(iterateNextLoadState));

router.route('/:id')
  .get(authMiddleware, asyncWrapper(getOneLoad))
  .put(authMiddleware, asyncWrapper(validateLoad), asyncWrapper(updateLoad))
  .delete(authMiddleware, asyncWrapper(deleteLoad));

router.post('/:id/post', authMiddleware, asyncWrapper(postLoad));

router.get('/:id/shipping_info', authMiddleware, asyncWrapper(getShippingInfo));

module.exports = router;