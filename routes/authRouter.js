const express = require('express');
const router = express.Router();

const { asyncWrapper } = require('./helpers/asyncHelpers');
const { validateRegistration, validateLogin, validateEnteredEmail } = require('./middlewares/validationMiddleware');
const { registration, login, resetPassword } = require('../controllers/authController');

router.post('/register', asyncWrapper(validateRegistration), asyncWrapper(registration));
router.post('/login', asyncWrapper(validateLogin), asyncWrapper(login));
router.post('/forgot_password', asyncWrapper(validateEnteredEmail), asyncWrapper(resetPassword));

module.exports = router;