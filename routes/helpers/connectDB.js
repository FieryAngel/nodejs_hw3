const mongoose = require('mongoose');
const { DB } = require('../../config');

const connectToDB = async () => {
  await mongoose.connect(DB, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });
};

module.exports = {
  connectToDB
};