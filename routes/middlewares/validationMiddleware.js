const Joi = require('joi');

const validateSchema = async (schema, data, next) => {
  try {
    await schema.validateAsync(data);
  }
  catch (err) {
    console.log(err.message);
    next(err);
  }

  next();
};

module.exports.validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().required()
      .email({ minDomainSegments: 2 })
      .messages({
        'string.empty': `'email' cannot be an empty field`,
        'any.required': `'email' is a required field`
      }),

    password: Joi.string()
      .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')).required().messages({
        'string.empty': `'password' cannot be an empty field`,
        'string.pattern.base': 'Invalid password! Password must be at least 6 characters',
        'any.required': `'password' is a required field`
      }),

    role: Joi.string()
      .pattern(new RegExp('^(SHIPPER|DRIVER)$')).required().messages({
        'any.required': `'role' is a required field`,
        'string.empty': `'role' cannot be an empty field`,
        'string.pattern.base': `Wrong role! Choose 'DRIVER' or 'SHIPPER'`
      })
  });

  validateSchema(schema, req.body, next);
};

module.exports.validateLogin = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().required()
      .messages({
        'string.empty': `'email' cannot be an empty field`,
        'any.required': `'email' is a required field`
      }),

    password: Joi.string().required().messages({
      'string.empty': `'password' cannot be an empty field`,
      'any.required': `'password' is a required field`
    })
  });

  validateSchema(schema, req.body, next);
};

module.exports.validateEnteredEmail = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().required()
      .messages({
        'string.empty': `'email' cannot be an empty field`,
        'any.required': `'email' is a required field`
      }),
  });

  validateSchema(schema, req.body, next);
};

module.exports.validateOldNewPasswords = async (req, res, next) => {
  const schema = Joi.object({
    oldPassword: Joi.string().required().messages({
      'string.empty': `'oldPassword' cannot be an empty field`,
      'any.required': `'oldPassword' is a required field`
    }),

    newPassword: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')).required().messages({
      'string.empty': `'newPassword' cannot be an empty field`,
      'string.pattern.base': 'Invalid new password! Password must be at least 6 characters',
      'any.required': `'newPassword' is a required field`
    })
  });

  validateSchema(schema, req.body, next);
};

module.exports.checkType = async (req, res, next) => {
  const schema = Joi.object({
    type: Joi.string().pattern(new RegExp('^(SPRINTER|SMALL STRAIGHT|LARGE STRAIGHT)$')).required()
      .messages({
        'string.empty': `'type' cannot be an empty field`,
        'string.pattern.base': `Wrong type! Choose 'SPRINTER', 'SMALL STRAIGHT' or 'LARGE STRAIGHT'`,
        'any.required': `'type' is a required field`
      }),
  });

  validateSchema(schema, req.body, next);
};

module.exports.validateLoad = async (req, res, next) => {
  const schema = Joi.object({
    name: Joi.string().required().messages({
      'string.empty': `'name' cannot be an empty field`,
      'any.required': `'name' is a required field`
    }),

    payload: Joi.number().positive().required().messages({
      'number.base': `'payload' must be a number`,
      'any.required': `'payload' is a required field`,
      'number.positive': `'payload' must be positive`
    }),

    pickup_address: Joi.string().required().messages({
      'string.empty': `'pickup_address' cannot be an empty field`,
      'any.required': `'pickup_address' is a required field`
    }),

    delivery_address: Joi.string().required().messages({
      'string.empty': `'delivery_address' cannot be an empty field`,
      'any.required': `'delivery_address' is a required field`
    }),

    dimensions: Joi.object({
      width: Joi.number().positive().required().messages({
        'number.base': `'width' must be a number`,
        'any.required': `'width' is a required field`,
        'number.positive': `'width' must be positive`
      }),

      length: Joi.number().positive().required().messages({
        'number.base': `'length' must be a number`,
        'any.required': `'length' is a required field`,
        'number.positive': `'length' must be positive`
      }),

      height: Joi.number().positive().required().messages({
        'number.base': `'height' must be a number`,
        'any.required': `'heigth' is a required field`,
        'number.positive': `'heigth' must be positive`
      })
    }).required().messages({
      'any.required': `'dimensions' is a required field`
    })
  });

  validateSchema(schema, req.body, next);
};
