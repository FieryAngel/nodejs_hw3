const express = require('express');
const morgan = require('morgan');
const app = express();

const port = process.env.PORT || 8080;

const authRouter = require('./routes/authRouter');
const userRouter = require('./routes/userRouter');
const truckRouter = require('./routes/truckRouter');
const loadRouter = require('./routes/loadRouter');

const { connectToDB } = require('./routes/helpers/connectDB');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);

app.use((err, req, res, next) => {
  res.status(400).json({ message: err.message });
});

const start = async () => {
  connectToDB();

  app.listen(port, () => {
    console.log(`Server works at port ${port}!`);
  });
};

start();