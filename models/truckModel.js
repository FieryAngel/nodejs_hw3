const mongoose = require('mongoose');

const truckSchema = mongoose.Schema({
  type: String,
  created_by: String,
  dimensions: Object,

  assigned_to: {
    type: String,
    default: null
  },

  created_date: {
    type: Date,
    default: Date.now(),
  },

  status: {
    type: String,
    default: 'IS'
  }
});

module.exports.Truck = mongoose.model('Truck', truckSchema);