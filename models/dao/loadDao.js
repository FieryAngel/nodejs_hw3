const { User } = require('../userModel');
const { Load } = require('../loadModel');
const { Truck } = require('../truckModel');
const { isDriver } = require('./truckDao');

const isShipper = async (user) => {
  if (user.role !== 'SHIPPER') {
    throw new Error('You are not a shipper');
  }
};

const existLoad = async (load) => {
  if (!load) {
    throw new Error('Load does not exist');
  }
};

const addLog = async (loadId, message) => {
  const log = new Object({
    message: message,
    time: new Date(Date.now()).toISOString()
  });

  await Load.findOneAndUpdate({ _id: loadId }, { $push: { logs: log } });
};

const createLoad = async (email, name, payload, pickup_address, delivery_address, dimensions) => {
  const user = await User.findOne({ email: email });

  await isShipper(user);

  const author = user._id;

  const load = new Load({
    created_by: author,
    name: name,
    payload: payload,
    pickup_address: pickup_address,
    delivery_address: delivery_address,
    dimensions: dimensions
  });

  await load.save();
};

const getDaoLoad = async (email, loadId) => {
  const user = await User.findOne({ email: email });

  let load;

  if (user.role === 'SHIPPER') {
    load = await Load.findOne({ _id: loadId, created_by: user._id }, { __v: 0 });
    await existLoad(load);
  } else {
    load = await Load.findOne({ _id: loadId, assigned_to: user._id }, { __v: 0 });
    await existLoad(load);
  }

  return load;
};

const updateDaoLoad = async (email, loadId, name, payload, pickup_address, delivery_address, dimensions) => {
  const user = await User.findOne({ email: email });

  await isShipper(user);

  const load = await Load.findOne({ _id: loadId, created_by: user._id }, { __v: 0 });

  await existLoad(load);

  if (load.status !== 'NEW') {
    throw new Error(`You are able to update only 'NEW' loads`);
  }

  await Load.findOneAndUpdate({ _id: loadId, created_by: user._id }, {
    $set: {
      name: name,
      payload: payload,
      pickup_address: pickup_address,
      delivery_address: delivery_address,
      dimensions: dimensions
    }
  });
};

const deleteDaoLoad = async (email, loadId) => {
  const user = await User.findOne({ email: email });

  await isShipper(user);

  const load = await Load.findOne({ _id: loadId, created_by: user._id }, { __v: 0 });

  await existLoad(load);

  if (load.status !== 'NEW') {
    throw new Error(`You are able to delete only 'NEW' loads`);
  }

  await Load.findOneAndDelete({ _id: loadId, created_by: user._id });
};

const isLoadFit = (load, truck) => {
  const isFit =
    load.dimensions.width < truck.dimensions.width &&
    load.dimensions.height < truck.dimensions.height &&
    load.dimensions.length < truck.dimensions.length &&
    load.payload < truck.dimensions.payload;

  return isFit;
};

const findTruck = async (loadId) => {
  const trucksList = await Truck.find({ status: 'IS' });

  if (!trucksList.length) {
    throw new Error('There are no trucks in system');
  }

  const load = await Load.findById(loadId);
  const validTruck = trucksList.find(truck => isLoadFit(load, truck) === true);

  return validTruck;
};

const postDaoLoad = async (email, loadId) => {
  const user = await User.findOne({ email: email });

  await isShipper(user);

  const load = await Load.findOne({ _id: loadId, created_by: user._id }, { __v: 0 });

  await existLoad(load);

  if (load.status !== 'NEW') {
    throw new Error(`You are able to post only 'NEW' loads`);
  }

  await Load.findOneAndUpdate({ _id: loadId, created_by: user._id }, { $set: { status: 'POSTED' } });
  await addLog(loadId, 'Load posted successfully');

  const truck = await findTruck(loadId);

  if (!truck) {
    await Load.findOneAndUpdate({ _id: loadId, created_by: user._id }, { $set: { status: 'NEW' } });
    await addLog(loadId, 'Driver not found');

    return null;
  }

  const driverId = truck.created_by;

  await Load.findOneAndUpdate({ _id: loadId, created_by: user._id }, { $set: { status: 'ASSIGNED', state: 'En route to Pick Up', assigned_to: driverId } });
  await addLog(loadId, `Assigned to driver with id: ${driverId}`);
  await Truck.findByIdAndUpdate(truck._id, { $set: { status: 'OL' } });
  await addLog(loadId, 'En route to Pick Up');

  return driverId;
};

const getActiveDaoLoad = async (email) => {
  const user = await User.findOne({ email: email });

  await isDriver(user);

  const load = await Load.findOne({ assigned_to: user._id, status: 'ASSIGNED' }, { __v: 0 });

  await existLoad(load);

  return load;
};

const changeLoadState = async (email) => {
  const user = await User.findOne({ email: email });

  await isDriver(user);

  const activeLoad = await getActiveDaoLoad(email);

  if (!activeLoad) {
    throw new Error('There is no active load');
  }

  let newState = '';

  switch (activeLoad.state) {
    case 'En route to Pick Up':
      newState += 'Arrived to Pick Up';
      break;
    case 'Arrived to Pick Up':
      newState += 'En route to delivery';
      break;
    case 'En route to delivery':
      newState += 'Arrived to delivery';
      break;
  }

  await Load.findOneAndUpdate({ assigned_to: user._id, status: 'ASSIGNED' }, {
    $set: { state: newState }
  });
  await addLog(activeLoad._id, newState);

  if (newState === 'Arrived to delivery') {
    await Load.findOneAndUpdate({ assigned_to: user._id, status: 'ASSIGNED' }, {
      $set: { status: 'SHIPPED' }
    });
    await addLog(activeLoad._id, 'Load shipped successfully');
    await Truck.findOneAndUpdate({ created_by: user._id, status: 'OL' }, { $set: { status: 'IS' } });
  }

  return newState;
};

const showShippingInfo = async (email, loadId) => {
  const user = await User.findOne({ email: email });

  await isShipper(user);

  const load = await Load.findOne({ _id: loadId, created_by: user._id }, { __v: 0 });

  if (load.status !== 'ASSIGNED') {
    throw new Error('Load is not active');
  }

  const truck = await Truck.findOne({ assigned_to: load.assigned_to }, { __v: 0 });

  const shippingInfo = new Object({
    load,
    truck
  });

  return shippingInfo;
};

const getAllLoads = async (email, status, offset, limit) => {
  const user = await User.findOne({ email: email });

  let loads;

  if (user.role === 'SHIPPER') {
    if (!status) {
      loads = await Load.find({ created_by: user._id }, { __v: 0 }).skip(offset).limit(limit);
    } else {
      loads = await Load.find({ created_by: user._id, status: status }, { __v: 0 }).skip(offset).limit(limit);
    }
  } else {
    if (!status) {
      loads = await Load.find({ assigned_to: user._id, $or: [{ status: 'ASSIGNED' }, { status: 'SHIPPED' }] }, { __v: 0 }).skip(offset).limit(limit);
    } else {
      if (status === 'ASSIGNED' || status === 'SHIPPED') {
        loads = await Load.find({ assigned_to: user._id, status: status }, { __v: 0 }).skip(offset).limit(limit);
      } else {
        throw new Error(`You are not able to get loads with status '${status}'`);
      }
    }
  }

  return loads;
};

module.exports = {
  createLoad,
  getDaoLoad,
  updateDaoLoad,
  deleteDaoLoad,
  postDaoLoad,
  getActiveDaoLoad,
  changeLoadState,
  showShippingInfo,
  getAllLoads
};
