const { Error } = require('mongoose');
const { Truck } = require('../truckModel');
const { User } = require('../userModel');

class Sprinter {
  constructor() {
    this.width = 300;
    this.length = 250;
    this.height = 170;
    this.payload = 1700;
  }
}

class SmallStraight {
  constructor() {
    this.width = 500;
    this.length = 250;
    this.height = 170;
    this.payload = 2500;
  }
}

class LargeStraight {
  constructor() {
    this.width = 700;
    this.length = 350;
    this.height = 200;
    this.payload = 4000;
  }
}

const isDriver = async (user) => {
  if (user.role !== 'DRIVER') {
    throw new Error('You are not a driver');
  }
};

const existTruck = async (truck) => {
  if (!truck) {
    throw new Error('Truck does not exist');
  }
};

const getDimensions = async (type) => {
  let dimensions;

  switch (type) {
    case 'SPRINTER':
      dimensions = new Sprinter();
      break;
    case 'SMALL STRAIGHT':
      dimensions = new SmallStraight();
      break;
    case 'LARGE STRAIGHT':
      dimensions = new LargeStraight();
      break;
  }

  return dimensions;
};

const createTruck = async (type, email) => {
  const user = await User.findOne({ email: email });

  await isDriver(user);

  const author = user._id;
  const dimensions = await getDimensions(type);

  const truck = new Truck({
    created_by: author,
    type: type,
    dimensions: dimensions
  });

  await truck.save();
};

const getDaoTrucks = async (email) => {
  const user = await User.findOne({ email: email });

  await isDriver(user);

  const trucks = await Truck.find({ created_by: user._id }, { __v: 0 });

  if (!trucks.length) {
    throw new Error('There are no trucks');
  }

  return trucks;
};

const getOneTruck = async (email, truckId) => {
  const user = await User.findOne({ email: email });

  await isDriver(user);

  const truck = await Truck.findOne({ _id: truckId, created_by: user._id }, { __v: 0 });

  await existTruck(truck);

  return truck;
};

const assignDaoTruck = async (email, truckId) => {
  const user = await User.findOne({ email: email });

  await isDriver(user);

  const truck = await Truck.findOne({ _id: truckId, created_by: user._id });

  await existTruck(truck);

  const assignedTruck = await Truck.findOne({ created_by: user._id, assigned_to: user._id, _id: { $ne: truckId } });

  if (assignedTruck) {
    throw new Error('You can assign only one truck');
  }

  if (truck.status === 'OL') {
    throw new Error('You are not able to assign truck while you are on load');
  }

  await Truck.findOneAndUpdate({ _id: truckId, created_by: user._id }, { $set: { assigned_to: user._id } });
};

const updateDaoTruck = async (email, truckId, type) => {
  const user = await User.findOne({ email: email });

  await isDriver(user);

  const truck = await Truck.findOne({ _id: truckId, created_by: user._id });

  await existTruck(truck);

  if (truck.assigned_to === user._id) {
    throw new Error('You are able to update only not assigned to you trucks');
  }

  const onLoadTruck = await Truck.findOne({ created_by: user._id, status: 'OL' });

  if (onLoadTruck) {
    throw new Error('You are able to change trucks info while you are on a load');
  }

  await Truck.findOneAndUpdate({ _id: truckId, created_by: user._id }, { $set: { type: type, dimensions: await getDimensions(type) } });
};

const deleteDaoTruck = async (email, truckId) => {
  const user = await User.findOne({ email: email });

  await isDriver(user);

  const truck = await Truck.findOne({ _id: truckId, created_by: user._id });

  await existTruck(truck);

  if (truck.assigned_to === user._id) {
    throw new Error('You are able to delete only not assigned to you trucks');
  }

  const onLoadTruck = await Truck.findOne({ created_by: user._id, status: 'OL' });

  if (onLoadTruck) {
    throw new Error('You are able to change trucks info while you are on a load');
  }

  await Truck.findOneAndDelete({ _id: truckId, created_by: user._id });
};

module.exports = {
  createTruck,
  getDaoTrucks,
  getOneTruck,
  assignDaoTruck,
  updateDaoTruck,
  deleteDaoTruck,
  isDriver
};
