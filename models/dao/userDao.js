const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const { User } = require('../userModel');
const { JWT_SECRET } = require('../../config');

const createUser = async (email, password, role) => {
  const existUser = await User.findOne({ email });

  if (existUser) {
    throw new Error('User with email ' +
      `${email} already exists`);
  }

  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role
  });

  await user.save();
};

const getUserByEmail = async (email) => {
  const user = await User.findOne({ email });

  if (!user) {
    throw new Error(`No user with email ` +
      `${email} found`);
  }

  return user;
};

const checkPassword = async (enteredPassword, userPassword) => {
  if (!(await bcrypt.compare(enteredPassword, userPassword))) {
    throw new Error('Wrong password!');
  }
};

const getToken = async (email, password) => {
  const user = await getUserByEmail(email);

  await checkPassword(password, user.password);

  const token = jwt.sign({ email: user.email }, JWT_SECRET);

  return token;
};

const getProfileInfo = async (email) => {
  await getUserByEmail(email);

  const user = await User.findOne({ email: email },
    { __v: 0, password: 0, role: 0 });

  return user;
};

const changeUserPassword = async (oldPassword, newPassword, email) => {
  const user = await getUserByEmail(email);

  await checkPassword(oldPassword, user.password);

  await User.findOneAndUpdate({ email: user.email },
    { $set: { password: await bcrypt.hash(newPassword, 10) } });
};

const deleteProfile = async (email) => {
  const user = await getUserByEmail(email);

  if (user.role !== 'SHIPPER') {
    throw new Error('Driver cannot delete a profile');
  }

  await User.findOneAndDelete({ email: user.email });
};

module.exports = {
  createUser,
  getUserByEmail,
  getToken,
  getProfileInfo,
  changeUserPassword,
  deleteProfile
};