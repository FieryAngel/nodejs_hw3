const mongoose = require('mongoose');

const loadSchema = mongoose.Schema({
  payload: Number,
  created_by: String,
  name: String,
  pickup_address: String,
  delivery_address: String,
  dimensions: Object,

  logs: {
    type: Array,
    default: []
  },

  state: {
    type: String,
    default: null
  },

  assigned_to: {
    type: String,
    default: null
  },

  created_date: {
    type: Date,
    default: Date.now(),
  },

  status: {
    type: String,
    default: 'NEW'
  }
});

module.exports.Load = mongoose.model('Load', loadSchema);