const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  email: {
    type: String,
    lowercase: true,
  },

  password: String,

  created_date: {
    type: Date,
    default: Date.now(),
  },

  role: String
});

module.exports.User = mongoose.model('User', userSchema);