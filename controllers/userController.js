const userDao = require('../models/dao/userDao');

module.exports.showProfile = async (req, res) => {
  const user = await userDao.getProfileInfo(req.user.email);

  res.json({ user: user });
};

module.exports.deleteProfile = async (req, res) => {
  await userDao.deleteProfile(req.user.email);

  res.json({ message: 'Profile deleted successfully' });
};

module.exports.changePassword = async (req, res) => {
  const { oldPassword, newPassword } = req.body;

  await userDao.changeUserPassword(oldPassword, newPassword, req.user.email);

  res.json({ message: 'Password changed successfully' });
};
