const userDao = require('../models/dao/userDao');

module.exports.registration = async (req, res) => {
  const { email, password, role } = req.body;

  await userDao.createUser(email, password, role);

  res.json({ message: 'Profile created successfully' });
};

module.exports.login = async (req, res) => {
  const { email, password } = req.body;

  res.json({ jwt_token: await userDao.getToken(email, password) });
};

module.exports.resetPassword = async (req, res) => {
  const { email } = req.body;

  await userDao.getUserByEmail(email);

  res.json({ message: 'New password sent to your email address' });
};
