const loadDao = require('../models/dao/loadDao');

module.exports.getLoads = async (req, res) => {
  const { status, offset, limit } = req.body;

  const loads = await loadDao.getAllLoads(req.user.email, status, offset, limit);

  res.json({ loads: loads });
};

module.exports.addLoad = async (req, res) => {
  const { name, payload, pickup_address, delivery_address, dimensions } = req.body;

  await loadDao.createLoad(req.user.email, name, payload, pickup_address, delivery_address, dimensions);

  res.json({ message: 'Load created successfully' });
};


module.exports.getActiveLoad = async (req, res) => {
  const load = await loadDao.getActiveDaoLoad(req.user.email);

  res.json({ load: load });
};

module.exports.iterateNextLoadState = async (req, res) => {
  const state = await loadDao.changeLoadState(req.user.email);

  res.json({ message: `Load state changed to ${state}` });
};

module.exports.getOneLoad = async (req, res) => {
  const load = await loadDao.getDaoLoad(req.user.email, req.params.id);

  res.json({ load: load });
};

module.exports.updateLoad = async (req, res) => {
  const { name, payload, pickup_address, delivery_address, dimensions } = req.body;

  await loadDao.updateDaoLoad(req.user.email, req.params.id, name, payload, pickup_address, delivery_address, dimensions);

  res.json({ message: 'Load details changed successfully' });
};

module.exports.deleteLoad = async (req, res) => {
  await loadDao.deleteDaoLoad(req.user.email, req.params.id);

  res.json({ message: 'Load deleted successfully' });
};

module.exports.postLoad = async (req, res) => {
  const driver = await loadDao.postDaoLoad(req.user.email, req.params.id);

  if (!driver) {
    return res.json({
      message: 'Load was not posted ',
      driver_found: false
    });
  }

  res.json({
    message: 'Load posted successfully',
    driver_found: true
  });
};

module.exports.getShippingInfo = async (req, res) => {
  const { load, truck } = await loadDao.showShippingInfo(req.user.email, req.params.id);

  res.json({ load, truck });
};
