const truckDao = require('../models/dao/truckDao');

module.exports.addTruck = async (req, res) => {
  const { type } = req.body;

  await truckDao.createTruck(type, req.user.email);

  res.json({ message: 'Truck created successfully' });
};

module.exports.getAllTrucks = async (req, res) => {
  const trucks = await truckDao.getDaoTrucks(req.user.email);

  res.json({ trucks: trucks });
};

module.exports.getTruck = async (req, res) => {
  const truck = await truckDao.getOneTruck(req.user.email, req.params.id);

  res.json({ truck: truck });
};

module.exports.updateTruck = async (req, res) => {
  const { type } = req.body;

  await truckDao.updateDaoTruck(req.user.email, req.params.id, type);

  res.json({ message: 'Truck details changed successfully' });
};

module.exports.deleteTruck = async (req, res) => {
  await truckDao.deleteDaoTruck(req.user.email, req.params.id);

  res.json({ message: 'Truck deleted successfully' });
};

module.exports.assignTruck = async (req, res) => {
  await truckDao.assignDaoTruck(req.user.email, req.params.id,);

  res.json({ message: 'Truck assigned successfully' });
};